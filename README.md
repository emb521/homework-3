# Homework 3

For this assignment you are to answer the following questions regarding grammars and parsers. This isn't a programming assignment per se, but you might want to experiment with some Rust code to answer some of the questions.

## Submission Instructions

1. Fork this repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Write answers to all of the questions. For parse trees, you may draw these by hand and scan them, draw them in powerpoint and make a picture, or make little ascii trees (probably the easiest). For example:

```
Root
| Parent1
  | Child1
  | Child2
    | GrandChild
  | Child3
| Parent2
```

6. When you're ready to submit indicate this on the corresponding assignment on course site.

## Questions

### Question 1

Write a grammar in EBNF notation to capture the following:

#### a. Strings in Rust. 

STRINGS:

characters = “a” | “b” | “c” | “d” | “e” | “g” | “h” | “i” | “j” | “k” | “l” | “m” | “n” | “o” | “p” | “q” | “r” | “s” | “t” | “u” | “v” | “w” | “x” | “y” | “z” | “0” | “1” | “2” |  “3” | “4” | “5” |“6” | “7” | “8” | “9” | ”!” | “@“ | “#” | “$” | “%” | “^” ;

letters = “a” | “b” | “c” | “d” | “e” | “g” | “h” | “i” | “j” | “k” | “l” | “m” | “n” | “o” | “p” | “q” | “r” | “s” | “t” | “u” | “v” | “w” | “x” | “y” | “z”;

numbers = “0” | “1” | “2” |  “3” | “4” | “5” |“6” | “7” | “8” | “9”;

escaped_quote =  ‘ “ ‘, characters, ‘ “\” ‘;

string = ‘ “ ‘, characters, ‘ “ ‘;


#### b. Comments in Rust. 

comments = one_line_comments | block_comments;

one_line_comments = “ // “, characters;

block_comments = “ /* ”, characters, “ */ “;


#### c. E-mail addresses. 

email_address = name , “ @ “, company, “ . “, organization_code, white_space, country_code;

name = characters;

company = characters;

organization_code = characters;

white_space = ? all visible characters ? ;

country_code = letters;


#### d. Phone numbers

phone_numbers =  n_digit, numbers, numbers, “-“, n_digit, numbers, numbers, “ - “, numbers, numbers, numbers, numbers;

n_digit = “2” | “3” | “4” | “5” | “6” | “7” | “8” | “9”; 


#### e. Numeric constants in Rust. 

binary_numbers = “0b”, binary;

octal_numbers = “0o”, numbers;

decimal_numbers = numbers;

hexadecimal_numbers = 0x, numbers, letters;

decimal_constants = 

real_numbers = 

binary= “0” | “1”;

//optional underscores in all numbers

### Question 2

Errors in a program can be classified according to when they are detected and, if they are detected at compile time, what part of the compiler detects them. Using Rust, give an example of each of the following:

1. A lexical error, detected by the scanner.
2. A syntax error, detected by the parser.
3. A static semantic error, detected by semantic analysis.
4. A ownership error, detected by the borrow checker.
5. A lifetime error, detected through lifetime analysis of variables.

Lexical Error: Scanner:


ind y = 8;

println!(“this is my integer“, y);



Syntax Error: Parser:

int y = 8

println!(“this is my integer“, y);


Static Semantic Error: Semantic Analysis:

int y = 8a;

println!(“this is my integer“, y);


Ownership Error: Borrow Checker: 

int x = 5;
int y = &x;
return (x);



### Question 3

In Rust, what is the limit on the value of a usize? What happens in the event of arithmetic overflow? Don't look this up, try and figure it out on your own. What are the implications of size limits on the portability of programs from one machine/compiler to another?

In Rust, what is the limit on the value of a usize? 

    -The limit on the value of usize is 128 bits (u128). 


What happens in the event of an arithmetic overflow? 

    -Rust exists the program we an error, by “panicking”. If you put it on into a mode that does not check for overflow and panic, Rust will do “two’s complement wrapping”. This is when the number you typed in is changed to the first number after the number that overflows it (for an integer, anything over 255). 

What are the implications of size limits on the portability of programs from one machine/compiler to another?

    -Size limits can effect the porting effort. Bigger sizes make portability harder.




### Question 4

Why is it difficult to tell whether a program is correct or not? 

It is difficult to tell if a program is correct or not because 


How do you personally go about finding bugs in your code?

I usually put in print lines to fine where something is going wrong. If that doesn’t work I guess and check to see what helps.


What kinds of bugs are revealed by testing?

You can check if certain functions and arithmetic changes produce the right outcome by checking the actually outcome with the predicted outcome.

What kind of bust cannot be caught by testing?

You can’t see stack overflow problems or syntax errors. It is also hard to write tests for everything if your program has a lot of code.


### Question 5

Consider the following grammar:

e stands for the string terminating character.

```ebnf
statement = assignment | subroutine_call;
assignment = identifier, ":=", expression;
subroutine_call = identifier, "(", argument_list, ")";
expression = primary, expression_tail;
expression_tail = operator, expression | e;
primary = identifier | subroutine_call | "(" , expression , ")";
operator = "+" | "-" | "*" | "/";
argument_list = expression, argument_tail;
argument_tail = "," , argument_list | e;
identifier = {a-z | A-Z | 0-9};
```

1. Construct a parse tree for the input string "print(x)".
2. Construct a parse tree for the input string "y := add(a,b)".
3. Construct a parse tree for the input string "z := 1 + 2 * 3".
4. Construct a parse tree for the input string "x := 9 * sin(x)".

### Question 6 

Consider the following context-free grammar:

```ebnf
G = G, B | G, N | e;
B = "(", E, ")";
E = E, "(", E, ")" | e;
N = "(", L, "]";
L = L, E | L, "(" | e;
```

1. Describe in English the language generated by this grammar. Hint: B stands for "balanced", N stands for "non-balanced".
2. Give a parse tree for the string "((]()".
3. Give a parse tree for the string "((()))".
